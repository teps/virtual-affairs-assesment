//
//  ScheduleTableViewCellViewModel.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleTableViewCellViewModel: ViewModel {

    //The date format that we will be using to display the dates
    let DATE_FORMAT="dd MMMM yyyy"

    //The two labels that are shown on the cell
    var topLabel = String()
    var bottomLabel = String()
    
    /**
     * This function will set a Schedule for this ViewModel
     *
     * @param A schedule object to associate with the cellview
     */
    func setSchedule(schedule : ScheduleModel){
        //Set the top label text
        topLabel="Begin: " +  DateUtil.convertDateToString(date: schedule.beginDate, format: DATE_FORMAT)
        //Set the end label text if we need to
        if let endDate = DateUtil.addDaysToDate(days: schedule.durationInDays, date: schedule.beginDate){
            bottomLabel="End: " + DateUtil.convertDateToString(date:endDate, format: DATE_FORMAT);
        }
        else{
            bottomLabel="End: Not available"
        }
        //Alert the delegate that values have changed
        alertDelegate()
    }
}
