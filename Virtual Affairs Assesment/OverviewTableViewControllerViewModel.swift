//
//  OverviewTableViewControllerViewModel.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class OverviewTableViewControllerViewModel: ViewModel {

    //This is the Set of schedules that this ViewModel is holding is starts empty
    public private(set) var schedules = [ScheduleModel]()
    
    /**
    * This function return the closure to be used when someone want to add a ScheduleModel to our OverviewTableViewControllerViewModel
    *
    * @return The closure to use to add a schedule to this ViewModel
    */
    public func addScheduleClosure() -> (_ :ScheduleModel) -> Void{
        return {schedule in
            //Add the schedule using out addSchedulefunction
            self.addSchedule(schedule: schedule)
        }
    }
    
    /**
     * This function retutn a disclore where we don't want to add, but edit. We should already contain the Schedule object.
     */
    public func editScheduleClosure(editSchedule: ScheduleModel) -> (_ :ScheduleModel)->Void{
        return {schedule in
            //Edit the editSchedule with the values passed back in schedule
            editSchedule.set(beginDate:  schedule.beginDate, durationInDays: schedule.durationInDays)
            //We need to alert the delegate, we have changes in the data
            self.alertDelegate()
        }
    }
    
    /**
     * This function help us add a ScheduleModel to our Set of models. 
     *
     * @param The Schedule model we would like to add
     */
    private func addSchedule(schedule: ScheduleModel){
        //Add the schedule into out array
        schedules.append(schedule)
        //When we finish putting not data, we need to notify that we have new data
        alertDelegate()
    }
}
