//
//  ScheduleModel.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleModel: NSObject {
    //The begin date for the schedule model 
    public private(set) var beginDate = Date()
    //The duration of the schedule in days. Default is always 7
    public private(set) var durationInDays = 7
    
    /**
     * This init function makes sure that we initialize the ScheduleModel object with a begin date and duration
     */
    init(beginDate: Date, duration: Int){
        //Set the begin date
        self.beginDate=beginDate
        //Set the duration
        self.durationInDays=duration
    }
    
    /**
     * This function will set both values the begin date and the duration date for this model 
     *
     * @param The begin date you would like to set
     * @param The duration in days you would like to set
     */
    public func set(beginDate : Date, durationInDays: Int){
        self.beginDate=beginDate
        self.durationInDays=durationInDays
    }
    
}
