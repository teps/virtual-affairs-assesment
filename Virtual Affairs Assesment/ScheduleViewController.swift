//
//  ScheduleViewController.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/28/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController, ViewModelDelegate {

    let VIEW_CONTROLLER_TITLE = "Schedule"
    
    //The view modal belonging to this ViewController
    var scheduleViewModel = ScheduleViewModel()
    //The callback function to send the new Schedule that our view controller has displaying when we finish
    var didFinish : (_ : ScheduleModel) -> Void = {schedule in }
    
    //The label that displays the begin value
    @IBOutlet weak var beginValueLabel: UILabel!

    //The label that displays the andValue
    @IBOutlet weak var endValueLabel: UILabel!
    
    //The date picker that the user is using to select the dates
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //The manual navigation bar, the view that we created, its not from the navigation view controller.
    @IBOutlet weak var manualNavigationBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set the title
        self.title = VIEW_CONTROLLER_TITLE
        //Set ourself as the delegate
        scheduleViewModel.setDelegate(toSetDelegate: self)
        //Update the UI
        updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Adjust the navigation bar depending if the viewcontroller is being pushed or presented modally
        adjustNavigationBar()
    }

    /**
     * This function is called when the view controller will disappear, in here we need to call out callback function notifying who ever assigned it that we are done and send them the new Schedule with the values our view ended up on.
     */
    override func viewWillDisappear(_ animated: Bool) {
        //Call the didFinish callback to tell whoever that is interested that we finished and senf them the ScheduleModel
        self.didFinish(scheduleViewModel.getScheduleModel())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clearClicked(_ sender: Any) {
        //This will reset the values
        scheduleViewModel.resetValues()
    }
    
    /**
     * The done button clicked, dissmiss self.
     *
     * @param The done button
     */
    @IBAction func doneClicked(_ sender: Any) {
        //Dissmiss the view controller
        self.dismiss(animated: true, completion: nil)
    }
    
    /**
     * The action called when the UIDatePicker value has changed.
     *
     * @param The date picker of who the value changed
     */
    @IBAction func dateValueChanged(_ sender: UIDatePicker) {
        //Set the set that the user selected
        scheduleViewModel.attemptSetBeginDate(setDate: sender.date)
    }

    /**
     * This is the implementation of the ScheduleViewModelDelegate protocol so we can get notified when the values change. This function should ony call update views.
     
     * @param The new value for the beginDateLabel
     * @param The new value fot the endDateLabel
     */
    func valuesDidChange() {
        updateUI()
    }

    /**
     * This is a helper function will hide the custom navigation bar if we don't need it. We wont need it if the view controller is being pushed on a navigation controller, we do need to when the view controller will be presented modally.
     */
    private func adjustNavigationBar(){
       if (!self.isBeingPresented) {
            // being pushed
            //Setup the clear button
            let clearButton = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ScheduleViewController.clearClicked(_:)))
            //Set the right button of the navigation bar to the clear button
            self.navigationItem.rightBarButtonItem = clearButton;
            //Hide the manual navigation bar 
            manualNavigationBar.isHidden = true
        }
    }
    
    /**
     * This function simply pulls the information out of the ViewModel and updates the UI elements
     */
    private func updateUI(){
        self.beginValueLabel.text = scheduleViewModel.beginDateStringValue
        self.endValueLabel.text = scheduleViewModel.endDateStringValue
        datePicker.setDate(scheduleViewModel.beginDate, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
