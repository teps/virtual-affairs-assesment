//
//  OverviewTableViewController.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class OverviewTableViewController: UITableViewController, ViewModelDelegate {

    let SCHEDULE_CELL_REUSE_ID = "scheduleCell"
    
    //The ViewModel for the OverviewTableViewController
    let overviewTableVCModel = OverviewTableViewControllerViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set the ViewModel its delegate to self
        overviewTableVCModel.delegate = self
    }

    /**
     * The user clicked to create a new schedule, we need to open the schedule picker view with this function.
     *
     * @param The button that was clicked.
     */
    @IBAction func addScheduleButtonClicked(sender: UIButton) {
        //Show the schedule picked with no start date
        showSchedulePickerWith(startDate: nil, finishClosure: overviewTableVCModel.addScheduleClosure())
    }
    
    /**
     * This function is the implementation of the ViewModelDelegate to get notified that values changed in the ViewModel
     */
    func valuesDidChange(){
        //Lets reload the tableview
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /**
     * This function will show the Schedule with an optional start date. If the startDate is set, the Schedule picker will have its defaults set to the given start date
     */
    private func showSchedulePickerWith(startDate :Date?, finishClosure :@escaping((ScheduleModel)->Void)){
        //Create the schedule picker view controller
        let schedulePicker = ScheduleViewController(nibName: "ScheduleViewController", bundle: nil)
        //Let the add Schedule handle the receiving of the
        schedulePicker.didFinish = finishClosure
        
        //We need to also set the start date if there is one
        if let startDate = startDate{
            schedulePicker.scheduleViewModel.attemptSetBeginDate(setDate: startDate)
        }
        
        //Show the picker view controller
        show(schedulePicker, sender: self)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        //We only need one section for this tableview
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the amount of schedules we have
        return overviewTableVCModel.schedules.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Get the schedule we want to show with this cell
        let toDisplay = overviewTableVCModel.schedules[indexPath.row]
        //Dequeue the cell with the given identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: SCHEDULE_CELL_REUSE_ID, for: indexPath) as! ScheduleTableViewCell
        //Get the cell its view model and pass it the Shedule model we want to associate with this cell
        cell.cellViewModel.setSchedule(schedule: toDisplay)
        //Return the cell
        return cell
    }
    
    /**
     * A cell cie whas been selected, we now neew to edit this Model
     */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Let's get  the schedule we will be editing
        let scheduleToEdit = overviewTableVCModel.schedules[indexPath.row]
        //We need to pass int he begin date so the picker can set its defaults
        showSchedulePickerWith(startDate: scheduleToEdit.beginDate, finishClosure: overviewTableVCModel.editScheduleClosure(editSchedule: scheduleToEdit))
        //Deselect the cell (for UI purposes)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
