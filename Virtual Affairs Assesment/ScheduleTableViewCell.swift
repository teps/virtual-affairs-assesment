//
//  ScheduleTableViewCell.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell, ViewModelDelegate {

    //The cell representing the beign label and the begin value
    @IBOutlet weak var topLabel: UILabel!
    //The cell representing the end label and the end value
    @IBOutlet weak var bottomLabel: UILabel!
    //The ViewModel for the Cell/View
    var cellViewModel = ScheduleTableViewCellViewModel();
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellViewModel.delegate=self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /**
     * This function is the implementation of the ViewModelDelegate to get notified that values changed in the ViewModel
     */
    func valuesDidChange(){
        //Update the UI with the new ViewModel values
        topLabel.text=cellViewModel.topLabel
        bottomLabel.text=cellViewModel.bottomLabel
    }
}
