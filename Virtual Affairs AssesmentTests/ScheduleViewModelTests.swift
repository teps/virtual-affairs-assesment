//
//  ScheduleViewModelTests.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/28/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import XCTest

class ScheduleViewModelTests: XCTestCase {
    
    var scheduleViewModelTest = ScheduleViewModel()

    let futureDate = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 10, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

    let pastDate = Calendar.current .date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 2010, month: 3, day: 10, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     * This test tests if everything was prperly set in init
     */
    func testAttemptSetBeginDate_PastDate(){
        scheduleViewModelTest.attemptSetBeginDate(setDate: pastDate!)
        XCTAssertNotEqual(pastDate, scheduleViewModelTest.beginDate, "The date should not be qual to the begin date, the date is a past date.")
        scheduleViewModelTest.attemptSetBeginDate(setDate: futureDate!)
        XCTAssertEqual(futureDate, scheduleViewModelTest.beginDate, "The begin date should be set to the date passed in, it was a future date.")
    }
    
    /**
     * This test tests the reset values function
     */
    func testResetValues(){
        scheduleViewModelTest.attemptSetBeginDate(setDate: futureDate!)
        scheduleViewModelTest.resetValues()
        XCTAssertNotEqual(scheduleViewModelTest.beginDate, futureDate, "Date was not properly reset when we called reset values")
    }
    
    /**
     * This test tests if the delegate is properly set when calling set delegate
     */
    func testSetDelegate(){
        class testClass : NSObject,  ViewModelDelegate{
            func valuesDidChange() {}
        }
    
        let testDelegate = testClass()
        
        scheduleViewModelTest.setDelegate(toSetDelegate: testDelegate)
        
        XCTAssert((testDelegate === scheduleViewModelTest.delegate), "The delegate was not set after we called setDelegate")
    }
    
    /**
     * This test tests if delegate get notified when we update the begin date
     */
    func testSetDelegateGetsNotified(){
        class testClass : NSObject,  ViewModelDelegate{
            var didGetNotified = false
            func valuesDidChange() {
                didGetNotified = true
            }
        }
        
        let testDelegate = testClass()
        
        scheduleViewModelTest.setDelegate(toSetDelegate: testDelegate)
        scheduleViewModelTest.attemptSetBeginDate(setDate: futureDate!)
        
        XCTAssert(testDelegate.didGetNotified, "The delegate was not notified after a value was chenged.")
    }
    
    /**
     * Test if the ViewModel return a correct ScheduleModelObject
     */
    func testGetScheduleModel(){
        scheduleViewModelTest.attemptSetBeginDate(setDate: futureDate!)
        let schedule = scheduleViewModelTest.getScheduleModel()
        
        XCTAssertEqual(schedule.beginDate, futureDate, "The ScheduleModel object does not contain the correct begin date")
        XCTAssertEqual(schedule.durationInDays, 7, "The ScheduleModel object does not duration, default is seven, it should be seven")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
