//
//  OverviewTableViewControllerViewModelTests.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import XCTest

class OverviewTableViewControllerViewModelTests: XCTestCase {
    
    var viewModel = OverviewTableViewControllerViewModel()

    let futureDate = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 10, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))
    
    let pastDate = Calendar.current .date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 2010, month: 3, day: 10, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     * This tests the adding of the schedule using the closure.
     */
    func testAddScheduleUsingTheClosure() {
        let closure = viewModel.addScheduleClosure()
        
        let futureSchedule = ScheduleModel(beginDate: futureDate!, duration: 3)
        let pastSchedule = ScheduleModel(beginDate: pastDate!, duration: 7)
        
        closure(futureSchedule)
        closure(pastSchedule)
        
        XCTAssert(viewModel.schedules.contains(futureSchedule), "The future eschedule was not added to the schedules array when using the closure.")
        XCTAssert(viewModel.schedules.contains(pastSchedule), "The past was not added to the schedules array when using the closure.")
    }
    
    /**
     * This function will test the edit schedule closure
     */
    func testEditScheduleClosure(){
        
        let testModel = ScheduleModel(beginDate: pastDate!, duration: 7)

        let closure = viewModel.editScheduleClosure(editSchedule: testModel)

        closure(ScheduleModel(beginDate: futureDate!, duration: 4))
 
        XCTAssertEqual(testModel.beginDate, futureDate!, "The date was not edited user the edit closure.")
        XCTAssertEqual(testModel.durationInDays, 4, "The duration was not edited user the edit closure.")

    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
