//
//  ScheduleModelTests.swift
//  Virtual Affairs Assessment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import XCTest

class ScheduleModelTests: XCTestCase {
    
    let futureDate = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 10, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTestInitWithBeginDateAndDuration() {
        let schedule = ScheduleModel(beginDate:futureDate!, duration:4)

        XCTAssertEqual(schedule.beginDate, futureDate!, "The init did not set the begin date correctly")
        XCTAssertEqual(schedule.durationInDays, 4, "The init did not set the duration date correctly")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
