//
//  OverviewTableViewController.swift
//  Virtual Affairs Assesment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import XCTest

class OverviewTableViewController: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testClickAddButton() {
        // The click on  the add button recording
        XCUIApplication().navigationBars["Overview"].children(matching: .button).element.tap()
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //let pickerTitle = XCUIApplication().

        //XCTAssertEqual(pickerTitle, "Schedule", "The add button did not open the Schedule picker view controller.")
    }
    
}
